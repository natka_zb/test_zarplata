<?php
namespace app\components;

use yii\httpclient\Client;

class ZarplataApi
{
    const URL = 'https://api.zp.ru/v1/';
    const METHOD_RUBRICS = 'rubrics';
    const METHOD_VACANCIES = 'vacancies';
    const GEO_NSK = 826;
    const PERIOD_TODAY = 'today';
    const LIMIT_DATA = 50;


    /**
     * @var integer
     */
    public $count = 0;

    /**
     * @var integer
     */
    public $limit = self::LIMIT_DATA;

    /**
     * @var integer
     */
    public $offset = 0;

    /**
     * @var integer
     */
    public $loadedCount = 0;

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
        $this->client->setTransport('yii\httpclient\CurlTransport');
    }

    /**
     * @param string $type
     * @param array $fields
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getData($type, $fields, $params = [])
    {
        $response = $this->client->createRequest()
            ->setUrl(self::URL . $type)
            ->setMethod('get')
            ->setData(array_merge($params, [
                'limit' => $this->limit,
                'offset' => $this->offset,
                'fields' => join(",", $fields)
            ]))
            ->send();

        if (!$response->isOk) {
            throw new \Exception("no connect " . self::URL);
        }

        $resultArray = json_decode($response->content, true);
        $this->limit = $resultArray['metadata']['resultset']['limit'] ?? self::LIMIT_DATA;
        $this->count = $resultArray['metadata']['resultset']['count'] ?? 0;
        $this->loadedCount += $this->limit;
        $this->offset++;
        return ($resultArray[$type] ?? []);
    }

    /**
     * @return boolean
     */
    public function isLoaded()
    {
        return ($this->loadedCount >= $this->count);
    }

    /**
     * @return boolean
     */
    public function isFirst()
    {
        return ($this->loadedCount === $this->limit);
    }

    /**
     * @return void
     */
    public function clear()
    {
        $this->limit = self::LIMIT_DATA;
        $this->count = 0;
        $this->loadedCount = 0;
    }
}