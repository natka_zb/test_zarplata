<?php
namespace app\components;

use Yii;

class Report
{
    const FILE_TYPE = 'xlsx';
    /**
     * @param array $report
     * @param string $key
     * @param int $value
     * @return void
     */
    public static function addIntoReport(&$report, $key, $value)
    {
        if (isset($report[$key])) {
            $report[$key] += $value;
        } else {
            $report[$key] = $value;
        }
    }

    /**
     * @param array $report
     * @param array $data
     * @return void
     */
    public static function addNewWordIntoReport(&$report, $data)
    {
        foreach ($data as $dataOne) {
            if (preg_match_all("/[\wа-яА-ЯёЁ_&\/\-']{3,}/u", $dataOne['header'], $matches)) {
                /*
                 * @todo возможно нужно ещё из распарсенных слов исключить ненужные слова, может быть предлоги или что ещё?
                 */
                foreach ($matches[0] as $key) {
                    $key = mb_strtolower($key);
                    if (isset($report[$key])) {
                        $report[$key] ++;
                    } else {
                        $report[$key] = 1;
                    }
                }
            }
        }
    }

    /**
     * @param array $report
     * @param string $name
     * @return void
     */
    public static function generateExcel(&$report, $name)
    {
        $dir = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR;
        self::prepareDir($dir);

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $activeSheet = $objPHPExcel->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(40);
        $activeSheet->getColumnDimension('B')->setWidth(20);
        $activeSheet->setTitle($name);

        $row = 1;
        foreach ($report as $key => $value) {
            $activeSheet->setCellValue('A' . $row, $key);
            $activeSheet->setCellValue('B' . $row, $value);
            $row++;
        }
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save($dir . $name . '_' . time() . '.' . self::FILE_TYPE);
    }

    /**
     * @param string $dir
     * @param integer $mode
     * @throws \Exception
     */
    private static function prepareDir($dir, $mode = 0755)
    {
        if (!is_dir($dir)) {
            if (!mkdir($dir, $mode, true)) {
                throw new \Exception('Не удалось создать папку "{dir}". Проверьте права доступа.');
            }
        }
    }
}