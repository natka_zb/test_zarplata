<?php
namespace app\commands;

use yii\console\Controller;
use app\components\ZarplataApi;
use app\components\Report;
use yii\helpers\Console;

/**
 * Class GetDataController
 * @package app\commands
 */
class GetDataController extends Controller
{
    public function actionGet()
    {
        $report1 = [];
        $report2 = [];
        $api = new ZarplataApi();
        $rubrics = [];

        try {
            do {
                $rubrics = array_merge($rubrics, $api->getData($api::METHOD_RUBRICS, ['id', 'title']));
            } while (!$api->isLoaded());
            $api->clear();

            $vacancies = [];
            foreach ($rubrics as $rubric) {
                do {
                    // https://api.zp.ru/v1/vacancies?geo_id=826&period=today&is_new_only=1&fields=id,header,rubrics
                    $vacancies = $api->getData($api::METHOD_VACANCIES,
                        [
                            'id',
                            'header'
                        ],
                        [
                            'geo_id' => $api::GEO_NSK,
                            'period' => $api::PERIOD_TODAY,
                            'is_new_only' => 1,
                            'rubric_id' => $rubric['id']
                        ]
                    );
                    if ($api->isFirst() && $api->count > 0) {
                        Report::addIntoReport($report1, $rubric['title'], $api->count);
                    }
                    Report::addNewWordIntoReport($report2, $vacancies);
                } while (!$api->isLoaded());
                $api->clear();
            }
            unset($vacancies);
            Report::generateExcel($report1, 'top rubrics');
            $this->stdout('Report 1 done' . PHP_EOL, Console::FG_GREEN);
            Report::generateExcel($report2, 'top words');
            $this->stdout('Report 2 done ' . PHP_EOL, Console::FG_GREEN);
        } catch (\Exception $e) {
            $this->stdout('We have an error: ' . $e->getMessage() . PHP_EOL, Console::FG_RED);
        }
    }
}